json.extract! post, :id, :title, :content, :email, :name, :created_at, :updated_at
json.url post_url(post, format: :json)
