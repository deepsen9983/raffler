Raffler.Views.Posts ||= {}

class Raffler.Views.Posts.IndexView extends Backbone.View
  template: JST["backbone/templates/posts/index"]

  initialize: () ->
    @collection.bind('reset', @addAll)

  addAll: () =>
    @collection.each(@addOne)
    console.log(@collection.each(@addOne)[1].get('name'))
    console.log(@collection.each(@addOne)[2].get('email'))
    

  addOne: (post) =>
    view = new Raffler.Views.Posts.PostView({model : post})
    @$("tbody").append(view.render().el)

  render: =>
    @$el.html(@template(posts: @collection.toJSON() ))
    @addAll()

    return this
