class Raffler.Models.Post extends Backbone.Model
  paramRoot: 'post'

  defaults:
    title: null
    content: null
    email: null
    name: null

class Raffler.Collections.PostsCollection extends Backbone.Collection
  model: Raffler.Models.Post
  url: '/posts'
